// smoothstate
$(function() {
    var $body = $('html, body'),
    content = $('#site-wrapper').smoothState({
        prefetch: true,
        pageCacheSize: 4,
        // blacklist anything you dont want targeted
        anchors: 'a',
        blacklist : '.wp-link',
        development : false,
        // Runs when a link has been activated
        onStart: {
            duration: 280, // Duration of our animation
            render: function (url, $container) {
                // toggleAnimationClass() is a public method
                // for restarting css animations with a class
                content.toggleAnimationClass('slide-out');
                // Scroll user to the top
                // $body.animate({
                //     scrollTop: 0
                // });
            }
        },
        onEnd : {
            duration: 0, // Duration of the animations, if any.
            render: function (url, $container, $content) {
                $body.css('cursor', 'auto');
                $body.find('a').css('cursor', 'auto');
                $container.html($content);
                // Trigger document.ready and window.load
                $(document).ready();
                $(window).trigger('load');
            }
        },
        onAfter : function(url, $container, $content) {
            $container.removeClass('slide-out');
            var $hash = $(window.location.hash);

            if ($hash.length !== 0) {

                var offsetTop = $hash.offset().top;

                $('body, html').animate({
                    scrollTop: (offsetTop - 60),
                }, {
                    duration: 280
                });
            }
        }
    }).data('smoothState');
    //.data('smoothState') makes public methods available
});
// functions
function mobileNav() {
  $("body .navbar-toggle").on("click", function() {
      if($("body").not(".mobile-open")) {
        $("body").removeClass("search-open").toggleClass("mobile-open");
      }
      $(".search-button").show();
      $(".navbar-toggle-2").hide();
    });
}
// fire ready
(function($, undefined) {
    var isFired = false;
    var oldReady = jQuery.fn.ready;
    $(function() {
        isFired = true;
        $(document).ready();
    });
    jQuery.fn.ready = function(fn) {
        if(fn === undefined) {
            $(document).trigger('_is_ready');
            return;
        }
        if(isFired) {
            window.setTimeout(fn, 1);
        }
        $(document).bind('_is_ready', fn);
    };
})(jQuery);
// Sage Routing
var Sage = {
    // All pages
    'common': {
      init: function() {   
        // JavaScript to be fired on all pages
        $('img').removeAttr('width').removeAttr('height');
        mobileNav();
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
         // Background Check
        // bgCheck();
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    'single_works' : {
      init: function() { 

      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery);
