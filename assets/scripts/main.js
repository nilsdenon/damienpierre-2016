/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */
// Mobile Nav
function mobileNav() {
  $("body .navbar-toggle").on("click", function() {
      if($("body").not(".mobile-open")) {
        $("body").removeClass("search-open").toggleClass("mobile-open");
        
      }
      if($('body').hasClass('mobile-open')) {
        // document.body.addEventListener('touchstart', function(e){ e.preventDefault(); });
      }
      $(".search-button").show();
      $(".navbar-toggle-2").hide();
    });
}
// Scroll to fixed
function scrollFixed() {

  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();    
    if (scroll > 50) {
     $("body").addClass('fixed');
   }
 });
}
// Remove width and height attributes from images
function removeWH() {
  $('img').removeAttr('width').removeAttr('height');
}
// Leave menu 
function leaveMenu() {
   $('#mobile-navigation a').on('click', function() {
       $("body").removeClass("mobile-open");
     });  
}
// On Ready
(function($) {
  

  // Blacklist Class
  function addBlacklistClass() {
    $('a').each( function() {
      if (this.href.indexOf('/wp-admin/') !== -1 || this.href.indexOf('/wp-login.php') !== -1 ) {
        $(this).addClass('wp-link');
      }
    });
  }
  // smoothstate
  $( function() {
    
    // main functions
    mobileNav();
    leaveMenu();
    addBlacklistClass();
    
    //'use strict';
    var settings = { 
     prefetch: true,
     cacheLength: 2,
      anchors: 'a',
      blacklist: '.wp-link .wpcf7-submit',
      scroll: true,
      onStart: {
        duration: 280,
        render: function ( $container ) {
          $container.addClass( 'slide-out' );
        }
      },
      onAfter: function( $container ) {
         $('html, body').stop().animate({ scrollTop: 0 }, 0);

        // reinitialize main functions
        mobileNav();
        leaveMenu();
        addBlacklistClass();

        var $hash = $( window.location.hash );

        if ( $hash.length !== 0 ) {

          var offsetTop = $hash.offset().top;

          $( 'body, html' ).animate( {
              scrollTop: ( offsetTop - 60 ),
            }, {
              duration: 280
          } );
        }

        $container.removeClass( 'slide-out' );
      }
    };

    $( '#site-wrapper' ).smoothState( settings );
  } );
  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
     
    // All pages
    'common': {
      init: function() {   

        // JavaScript to be fired on all pages
        //$('img').removeAttr('width').removeAttr('height');
        // Preloader
        $(window).load(function() { 
			// $('#status').fadeOut();
			// $('#preloader').delay(350).fadeOut('slow'); 
			// $('body').delay(350).css({'overflow':'visible'});

     
		});

  
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
         // Background Check
       // bgCheck();
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.

