$(function() {
	// $.scrollify({
	// 	section : ".scroll-section",
	// 	easing: "easeOutExpo",
	// });
});
$( window ).ready(function() {
  
    var wHeight = $(window).height();

    $('.scroll-section')
      .height(wHeight)
      .scrollie({
        scrollOffset : -10,
        scrollingInView : function(elem) {
                   
          var bgColor = elem.data('background');
          
          $('body').css('background-color', bgColor);
          
        }
      });

  });