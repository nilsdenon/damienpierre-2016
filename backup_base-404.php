<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body>
  
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    404 page
    <main id="dmp-main"role="document">
      <?php include Wrapper\template_path(); ?>
    </main><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
