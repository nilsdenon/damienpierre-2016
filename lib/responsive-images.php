<?php

if ( ! isset( $content_width ) ) {
    $content_width = 850;
}

add_image_size( 'smartphone_small', 320, 9999 );

add_image_size( 'smartphone_large', 480, 9999 );

add_image_size( 'vignette', 400, 9999 );

add_image_size( 'banner', 1200, 550 );

add_image_size( 'content', 850, 9999 );

add_image_size( 'backdrop', 1440, 900 );

// all images
function responsive_image($image, $alt='', $class = '', $size='') {

    if (!empty($image)) {
        if (!$alt) {
            $alt = $image['alt'];
        }
                
        $url = $image['url'];
        
        if ($size) {
            if (isset($image['sizes'][$size])) {
                $url = $image['sizes'][$size];
            }           
        }
        
        if (function_exists('wp_get_attachment_image_srcset')) { 
            $img = '<img src="'. $url . '" srcset="' . wp_get_attachment_image_srcset( $image['id'], $size ) . '" alt="' . $alt . '"';
        } else {
            $img = '<img src="'. $url . '" alt="' . $alt . '"';
        }
        
        if ($class) { 
            $img .= ' class="' . $class . '"';
        }
        $img .= ' />';
        
        return $img;
    }   
}

add_filter( 'acf_the_content', 'wp_make_content_images_responsive' );

// Remove width and height attributes on images
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}
