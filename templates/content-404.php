<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webfolio
 */

?>


<!-- <?php 
    $image = get_field('hero_404', 'option');
    $alt = $image['alt'];
    $size = 'backdrop';

    if($image): 
?>
    <ul class="slideshow">
     <li>
        <span class="slideshow-image-container backdrop-cover">

          <?php { echo responsive_image( $image, $alt, $size, $size); }
          ?>
            </span>
        </li>
    </ul>
<?php endif; ?> -->


<?php 
    
    $imageFG = get_field('backdrop_404_foreground', 'option');
    $altFG = $imageFG['alt'];
    
    $imageBG = get_field('backdrop_404_background', 'option');
    $altBG = $imageBG['alt'];

   

    $size = 'backdrop';

    if($image): 
?>
    <div class="backdrop-404">
     
       

          <?php { echo responsive_image( $imageFG, $altFG, 'img-404-fg', $size); }
          ?>
   
       
    </div>
<?php endif; ?>