<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webfolio
 */

?>


<nav class="project-navigation">
	<?php next_post_link( '%link', '<svg class="arrow arrow-left" viewBox="0 0 11.2 20" enable-background="new 0 0 11.2 20">
		<path d="M0 10l10 10 1.2-1.2-8.7-8.8 8.7-8.8-1.2-1.2-10 10z"/>
	</svg><span>Previous Project</span>' );?>
	
	<?php if(get_field('project_status')): ?>
		
		<span class="status">Offline</span>

		<?php else: ?>
			
			<?php if(get_field('project_url')): ?>     
				<a href="http://<?php the_field('project_url'); ?>" target="_blank" class="button secondary icon-live">Live</a>
			<?php endif; ?> 

	<?php endif; ?> 

	<?php previous_post_link( '%link', '<svg class="arrow arrow-right" viewBox="0 0 11.2 20" enable-background="new 0 0 11.2 20">
		<path d="M11.2 10l-10 10-1.2-1.2 8.8-8.8-8.8-8.8 1.2-1.2 10 10z"/>
	</svg>
	<span>Next Project</span>' );?>
</nav>