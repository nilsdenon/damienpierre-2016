
<li>
  

  <figure <?php post_class(); ?>> 

  <figcaption class="overlay">
      <div class="overlay-top">
        <?php if(get_field('accent_color')): ?>
          <h3 style='color: <?php the_field('accent_color'); ?>;'>
        <?php else: ?>
          <h3>
        <?php endif; ?>
        <?php the_title(); ?></h3>
        <?php if(get_field('project_type')): ?>
       <h4><?php the_field('project_type'); ?></h4>
     <?php endif; ?>
       </div>
       <div class="overlay-bottom">
        <a href="<?php the_permalink(); ?>" class="button secondary">View Project</a>
      </div>

  </figcaption>
  <?php if(get_field('vignette_background_image')): ?>

    <?php 
        $image = get_field('vignette_background_image');
        $size = 'vignette'; 
        $alt = $image['alt'];
        if( $image ) {
            echo responsive_image( $image, $alt, $size, $size);
        }
    ?>
     
    <?php endif; ?>


</figure>
</li>