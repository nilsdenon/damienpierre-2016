

<?php if(get_field("frontpage_post_type_order", "option") == "random"): ?>

  <?php
    $currentID = get_the_ID();
    $loop = new WP_Query(array(
    'post_type' => 'works',
    'posts_per_page' => 1,
    'category' => 'current',
    'orderby' => 'rand',
    // exclude trauerkartenstore from loop
    'post__not_in' => array( 1282)
    )); ?>

    <?php else: ?>

    <?php $loop = new WP_Query(array(
    'post_type' => 'works',
    'posts_per_page' => 1,
    'category' => 'current'
    )); ?>

<?php endif; ?>


     <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

    <ul class="slideshow">



            <li>
                <?php if(get_field('backdrop')): ?>
                <figure class="post-<?php the_ID(); ?>">


                    <?php if(get_field('caption_position')): ?>
                    <figcaption class="<?php the_field('caption_position'); ?>" style="background-color:rgba(255,255,255, <?php the_field('overlay_transparency_1'); ?>);">
                    <?php else: ?>
                    <figcaption style="background-color:rgba(255,255,255, <?php the_field('overlay_transparency_1'); ?>;)">
                    <?php endif; ?>

                        <h1>
                            <?php if(get_field('accent_color')): ?>
                            <a style='color: <?php the_field('accent_color'); ?>;' href="<?php the_permalink(); ?>">
                            <?php else: ?>
                            <a href="<?php the_permalink(); ?>">
                            <?php endif; ?>

                            <?php the_title(); ?></a></h1>
                        <?php if(get_field('project_type')): ?>
                            <h2>
                                <a href="<?php the_permalink(); ?>">
                                <?php the_field('project_type'); ?></a></h2>
                        <?php endif; ?>
                        <a href="<?php the_permalink(); ?>" class="button secondary">View Project</a>
                    </figcaption>

                    <?php if(get_field('backdrop_option')): ?>
                    <span class="slideshow-image-container <?php the_field('backdrop_option') ?>">
                        <?php else: ?>
                      <span class="slideshow-image-container">
                    <?php endif; ?>
                   <?php
                    $image = get_field('backdrop');
                    $size = 'backdrop';
                    $alt = $image['alt'];

                    if ($image) { echo responsive_image( $image, $alt, $size, $size); }
                    ?>
                    </span>

                    <span class="bgColor" style='background-color:<?php the_field('background_color') ?>;'></span>
                </figure>
                <?php endif; ?>
            </li>

    </ul>
 <?php endwhile; wp_reset_query(); ?>
