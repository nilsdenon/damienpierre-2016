<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webfolio
 */

?>


<div class="project-details">
	<dl>
		<?php if(get_field('project_client')): ?>
		
			<dt>Client:</dt>
			<dd><?php the_field('project_client'); ?></dd>
		
		<?php endif; ?>
	
		<?php if(get_field('project_task')): ?>
			
			<dt>Services provided:</dt>
			<dd><?php the_field('project_task'); ?></dd>
		
		<?php endif; ?>

		<?php if(get_field('obility_project')): ?>
			
			<dt>Programming:</dt>
			<dd>Obility GmbH</dd>
		
		<?php endif; ?>

		</dl>

		<?php 
			$terms = get_the_terms($post->ID, 'technology');

			if(is_array($terms)) {
				if (taxonomy_exists('technology')) {
					echo '<dl>';
					echo '<dt>Technologies involved:&nbsp;</dt>';
					echo '<dd>';
				//echo get_the_term_list( $post->ID, 'discipline', '<ul class="project-disciplines tag-list"><li>', '</li><li>', '</li></ul>' );
				// echo get_the_term_list( $post->ID, 'technology', '<ul class="project-technologies tag-list"><li>', '</li><li>', '</li></ul>' );
				// echo '</dd>';
				// echo '</dl>';

					echo strip_tags (
						get_the_term_list( get_the_ID(), 'technology', ""," / " )
						);
					echo '</dd>';
					echo '</dl>';
				}
			}
		?>
	
	
		<?php if(get_field('project_description')): ?>  
			<?php the_field('project_description'); ?>
		<?php endif; ?> 
	
</div>

