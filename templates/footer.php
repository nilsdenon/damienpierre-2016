<footer id="dmp-footer">
    <div class="container">
        <nav id="tertiary-nav">
            <p id="credits">
                <?php _e( 'Copyright &copy; ' ); ?>
                <?php echo date( 'Y' ); ?> <strong>
                <?php bloginfo( 'name' ); ?>
                </strong>
                <?php _e( 'All rights reserved.' ); ?>
            </p>
            <?php if ( has_nav_menu( 'secondary_navigation' ) ) : ?>
            <?php
            wp_nav_menu( array(
                'theme_location' => 'secondary_navigation',
                'menu_class'     => 'social-links-menu',
                'depth'          => 1,
                'link_before'    => '<span class="screen-reader-text">',
                'link_after'     => '</span>',
            ) );
        ?>
            <?php endif; ?>
        </nav>
    </div>
</footer>
</div>
</div>