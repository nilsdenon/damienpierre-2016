<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webfolio
 */

?>
	

<div <?php post_class('project-hero single hero cf'); ?> style="background-image:url(<?php the_field('hero_background_image') ?>);">

</div>

<?php if(is_singular('works')) :?>
 <nav class="project-navigation">
   <?php previous_post_link( '%link', '<svg viewBox="0 0 11.2 20" enable-background="new 0 0 11.2 20">
    <path d="M0 10l10 10 1.2-1.2-8.7-8.8 8.7-8.8-1.2-1.2-10 10z"/>
  </svg><span>Previous Project</span>' );?>

   <?php next_post_link( '%link', '<svg viewBox="0 0 11.2 20" enable-background="new 0 0 11.2 20">
    <path d="M11.2 10l-10 10-1.2-1.2 8.8-8.8-8.8-8.8 1.2-1.2 10 10z"/>
  </svg><span>Next Project</span>' );?>

</nav>
<?php endif; ?>

  <h2><?php the_title(); ?></h2>

   <?php if(get_field('project_type')): ?>
        <h3><?php the_field('project_type'); ?></h3>
      <?php endif; ?>