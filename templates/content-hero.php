<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webfolio
 */

?>


<div <?php post_class('project-hero hero cf'); ?>>


  
<?php if(get_field('background_color')): ?>  
<figure style="background-color: <?php the_field('background_color'); ?>;">
<?php else: ?>
<figure>
<?php endif; ?>
    

    <div class="bannerWrapper">
    <?php 
        $image = get_field('hero_background_image');
        $size = 'banner';  

        if ($image) { echo responsive_image( $image, $size); }
      ?>
    </div>
  
    <div class="captionWrapper">
    <?php if(get_field('overlay_transparency_2')): ?>
    <figcaption style="background-color:rgba(255,255,255, <?php the_field('overlay_transparency_2'); ?>);">
    <?php else: ?>
    <figcaption>
    <?php endif; ?>
    

    <?php if(get_field('accent_color')): ?>
      <h1 style='color: <?php the_field('accent_color'); ?>;'>
      <?php else: ?>
        <h1>
    <?php endif; ?>

    <?php the_title(); ?></h1>
    <?php if(get_field('project_type')): ?>
      <h2><?php the_field('project_type'); ?></h2>
    <?php endif; ?>



  <?php if(is_archive('works')) :?>
   <a href="<?php the_permalink(); ?>" class="button secondary">View Project</a>

  <?php endif; ?> 

  </figcaption>
  </div>


 
 
</figure>
  
</div>

