<div id="site-wrapper" class="m-scene">



<?php if (is_front_page()): ?>
    
    <?php 
    $loop = new WP_Query(array( 
        'post_type' => 'works', 
        'posts_per_page' => 1, 
        'category' => 'current'
        )); 
        ?>

        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

            <?php if(get_field('ui') == "dark"): ?>

                <div id="page" <?php body_class("background--dark"); ?>>

            <?php elseif(get_field('ui') == "light"): ?>

                <div id="page" <?php body_class("background--light"); ?>>

            <?php elseif(get_field('ui') == "complex"): ?>

                <div id="page" <?php body_class("background--complex"); ?>>


            <?php elseif(get_field('ui') == "special"): ?> 
                
                <?php if(get_field('accent_color')): ?> 
                 <style type="text/css">

                    .logo path, .menu-target path,
                    .mobile-open .logo path, .mobile-open .menu-target path {
                        fill: <?php the_field('accent_color'); ?> !important;
                    }
                    .branding-details,
                    #dmp-header nav a,
                    #credits,
                    #dmp-footer a {
                        color: <?php the_field('accent_color'); ?>;
                    }
                    .nav .active a:after {
                        background-color: <?php the_field('accent_color'); ?>;
                    }
                </style>
                 <?php endif; ?>
            
                <div id="page" <?php body_class("background--special"); ?>>  

            <?php else: ?>

                <div id="page" <?php body_class(); ?> style='background-color:<?php the_field('background_color') ?>;'>

            <?php endif; ?>

        <?php endwhile; wp_reset_query(); ?>


        <?php else: ?>
            <?php if(is_404()): ?>

                <div id="page" <?php body_class("background--dark"); ?>>

                <?php else: ?>
                
                <div id="page" <?php body_class(); ?> >

            <?php endif; ?>        
        

<?php endif; ?>
  
<header id="dmp-header" class="navbar" role="banner">
    <div class="header-container header-pinned">
        <div class="container"> <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
            <svg class="logo"  viewBox="-275 400.9 43.6 40" enable-background="new -275 400.9 43.6 40">
                <path d="M-254.4 421.7M-254.4 421.7"/>
                <path class="st0" d="M-275 401.5l22.8 15.5-22.8 15.2zM-254.4 421.7M-254.4 421.7"/>
                <path class="st0" d="M-254.8 428.8l18-12-14.7-10v5.7l-3.3-2.2v-9.4l23.4 15.9-20.1 13.4v8.8l-3.3 1.9z"/>
            </svg>
            <span class="branding-details"><strong>
                <?php bloginfo('name'); ?>
            </strong>
            <?php bloginfo('description'); ?>
        </span></a>
        <nav id="main-nav" class="nav" role="navigation">
            <?php if ( has_nav_menu( 'primary_navigation' ) ) : ?>
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'primary_navigation',
                    'menu_id'    => 'menu-primary-navigation',
                    'menu_class'     => 'nav navbar-nav',
                    'depth'          => 1,

                    ) );
                    ?>
                <?php endif; ?>
                <ul id="tool-nav" class="nav">
                    <li class="menu-item menu-item-mobile-nav">
                        <button type="button" class="navbar-toggle">
                            <svg class="menu-target" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" enable-background="new 0 0 30 30">
                            <g class="target">
                                <path d="M15,0C6.7,0,0,6.7,0,15s6.7,15,15,15s15-6.7,15-15S23.3,0,15,0z M15,27C8.4,27,3,21.6,3,15S8.4,3,15,3s12,5.4,12,12
    S21.6,27,15,27z"/>
                            </g>
                            <g class="cross">
                                <path class="cross-left" d="M20.4,21.9c-0.4,0-0.8-0.1-1.1-0.4L8.5,10.6c-0.6-0.6-0.6-1.5,0-2.1c0.6-0.6,1.5-0.6,2.1,0l10.8,10.8c0.6,0.6,0.6,1.5,0,2.1
    C21.2,21.8,20.8,21.9,20.4,21.9z"/>
                                <path class="cross-right" d="M9.6,21.9c-0.4,0-0.8-0.1-1.1-0.4c-0.6-0.6-0.6-1.5,0-2.1L19.4,8.5c0.6-0.6,1.5-0.6,2.1,0c0.6,0.6,0.6,1.5,0,2.1L10.6,21.5
    C10.4,21.8,10,21.9,9.6,21.9z"/>
                            </g>
                            <g class="bullseye">
                                <path d="M15,10.5c-2.5,0-4.5,2-4.5,4.5s2,4.5,4.5,4.5s4.5-2,4.5-4.5S17.5,10.5,15,10.5z"/>
                            </g>
                            
                        </svg>
                        </button>
                    </li>
                    <!-- <li>
                        <button class="search-button" type="button">
                           
                    </button>
                        <button style="display:none;" type="button" class="navbar-toggle-2"> <span></span> </button>
                    </li> -->
                </ul>
            </nav>
        </div>
    </div>
    <nav id="mobile-navigation">
        <?php if ( has_nav_menu( 'primary_navigation' ) ) : ?>
            <?php
            wp_nav_menu( array(
              'theme_location' => 'primary_navigation',
      //'menu_id'    => 'menu-primary-navigation',
              'menu_class'     => 'nav navbar-nav',
              'depth'          => 1,

              ) );
              ?>
          <?php endif; ?>
      </nav>
      <?php get_search_form(); ?>
  </header>