
<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>
      <?php get_template_part( 'templates/content', 'hero' ); ?>  
  <?php endwhile; ?>   
<?php endif; ?>

<div class="container">


<?php while (have_posts()) : the_post(); ?>

   <?php if(is_singular('works')) :?>
        
         

         <?php get_template_part( 'templates/content', 'navigation' ); ?>  

       <?php endif; ?>
       
  <article id="project-container" <?php post_class(get_field('remove_margin')); ?>>
    
      <?php get_template_part( 'templates/content', 'details' ); ?>

     <?php if( have_rows('work_content') ): ?>

      <?php while( have_rows('work_content') ): the_row(); 

        // vars
        
        $image = get_sub_field('work_image');
       
        $alt = $image['alt'];

        $cap = $image['caption'];

        $desc = $image['description'];

        $size = 'content';

      ?>
    
      <section class="project-box cf">
        <?php if(get_sub_field('work_image')): ?>

          <figure>
            
            <?php if ($image) { echo responsive_image( $image, $alt, $size, $size); } ?>

           
            
            <?php if($cap || $desc): ?>
              <figcaption>
                <?php if($cap): ?>
                  <span class="cap"><?php echo $cap ?></span>
                <?php endif; ?>

                <?php if($desc): ?>
                 <span class="desc"><?php echo $desc ?></span>
                <?php endif; ?>
              </figcaption>
            <?php endif; ?>

           

          </figure>

        <?php endif; ?>

        <?php if(get_sub_field(' work_image_description')): ?>

          <p><?php get_sub_field('work_image_description'); ?></p>

        <?php endif; ?>  

      </section>


    <?php endwhile; ?>


    <?php endif; ?>

    <?php if(get_field('custom_code') && get_field('custom_html_code')): ?>
    <section class="project-box cf">
        <?php the_field('custom_html_code'); ?>
    </section>
    <?php endif; ?>  

     <!-- related posts -->
     <?php if(get_field('related_posts')): ?>
        <?php get_template_part( 'templates/content', 'related' ); ?>  
      <?php endif; ?>


      <!-- related posts -->

  </article>
   <?php if(is_singular('works')) :?>

         <?php get_template_part( 'templates/content', 'navigation' ); ?>  

       <?php endif; ?>
<?php endwhile; ?>
</div>
