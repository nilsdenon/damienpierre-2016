<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webfolio
 */

?>
<?php 

$posts = get_field('related_posts');

if( $posts ): ?>
<section class="related-works">
	<h2>Related Projects:</h2>
	<div class="container">
		<ul class="project-list">

			<?php 

				foreach ($posts as $post): 

				$image = get_field('vignette_background_image', $post->ID);
				$size = 'vignette'; 
				$alt = $image['alt'];

				 setup_postdata($post);
			?>
			<li>


				<figure <?php post_class(); ?>> 

					<figcaption class="overlay">

						<div class="overlay-top">
							<?php if(get_field('accent_color')): ?>
								<h3 style='color: <?php the_field('accent_color'); ?>;'>
								<?php else: ?>
									<h3>
									<?php endif; ?>
									<?php the_title(); ?></h3>
									<?php if(get_field('project_type')): ?>
										<h4><?php the_field('project_type'); ?></h4>
									<?php endif; ?>
								</div>
								<div class="overlay-bottom">
									<a href="<?php the_permalink(); ?>" class="button secondary">View Project</a>
								</div>

							</figcaption>
							<?php if(get_field('vignette_background_image')): ?>

								<?php 
								$image = get_field('vignette_background_image');
								$size = 'vignette'; 
								$alt = $image['alt'];
								if( $image ) {
									echo responsive_image( $image, $alt, $size, $size);
								}
								?>

							<?php endif; ?>


						</figure>
					</li>
					
					<?php endforeach; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
			</div>
		</section>    
	<?php endif; ?>