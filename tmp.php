<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class("post-work"); ?>>
    

   <?php if( have_rows('content') ): ?>

    <?php while( have_rows('content') ): the_row(); 

     

   ?>


    <section class="content-block">

      <?php if (get_sub_field('image')) { echo _acf_ricg_image( get_sub_field('image'), 'large'); } ?>

    </section>

  <?php endwhile; ?>

  <?php endif; ?>

    <footer class="post-info">
      <?php if(get_field('meta')): ?>
        <p class="info"><?php the_field('meta'); ?></p>
      <?php endif; ?>
       
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    
  </article>
<?php endwhile; ?>























<?php if( have_rows('work_content') ): ?>

  <?php while( have_rows('work_content') ): the_row(); 

    // vars
    $size = 'full';

  ?>

    <section class="project-box cf">
      <?php if(get_sub_field('work_image')): ?>
        
        <figure>
        
          <?php echo wp_get_attachment_image( $image, $size ); ?>
          
          <?php if(get_sub_field('work_image_caption')): ?>
            <figcaption><?php get_sub_field('work_image_caption'); ?></figcaption>
          <?php endif; ?>
        
        </figure>

      <?php endif; ?>
      
      <?php if(get_sub_field(' work_image_description')): ?>
          
          <p><?php get_sub_field('work_image_description'); ?></p>

      <?php endif; ?>  

    </section>

  <?php endwhile; ?>
  
<?php endif; ?>









