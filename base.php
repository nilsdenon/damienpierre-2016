<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>

  <?php if (is_admin_bar_showing()): ?>
  <body class="<?php echo 'adminbar logged-in' ?>">
  <?php endif; ?>
   
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <main id="dmp-main" class="scene_element" role="document">
      <div class="container">
      <?php if (is_front_page()):?>
          
          <?php get_template_part('templates/content', 'frontpage'); ?>
        
        <?php elseif(is_404()) :?>
          
          <?php get_template_part( 'templates/content', '404' ); ?>

        <?php elseif(is_archive()) :?>
          Archive
        <?php elseif(is_single()) :?>
          Single
         <?php elseif ( 'works' == get_post_type()): ?>
            <?php if(is_archive()) :?>
              Archive Works
              <?php elseif(is_single()) :?>
                Single Works
            <?php endif; ?>
        <?php endif; ?>    
      <?php include Wrapper\template_path(); ?>
      </div>
    </main><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
