
<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<?php if (have_posts()) : ?>
  <?php $count = 0; ?>
  <?php while (have_posts()) : the_post(); ?>
    <?php $count++; ?>
    <?php if ($count == 1) : ?>

          <?php get_template_part( 'templates/content', 'hero' ); ?>
       
    <?php endif; ?>
  <?php endwhile; ?>   
<?php endif; ?>
<div class="container">
<?php if (have_posts()) : ?>
  <?php $count = 0; ?>
  <ul class="project-list">
    <?php while (have_posts()) : the_post(); ?>
      <?php $count++; ?>
      <?php if ($count !== 1) : ?>
        <?php get_template_part( 'templates/content', 'works' ); ?>    
      <?php endif; ?>
    <?php endwhile; ?>
  </ul>
<?php endif; ?>
</div>
<!-- <?php the_posts_navigation(); ?> -->

